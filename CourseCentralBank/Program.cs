﻿using System;
using static TryCatchLesson.CurseFromCentralBank;

namespace TryCatchLesson
{
    class Program
    {        
        static void Main()
        {
            while (true)
            {
                var course = new CurseFromCentralBank();
                course.FormatCourse();
                course.Notify += course.Messege;
                PrintDel printCurseDel;
                printCurseDel = course.PrintCourse;
                printCurseDel += course.WriteCourseOnDevice;
                Console.WriteLine("--------------------------------------------");
                printCurseDel(course.courses);
                Console.WriteLine("--------------------------------------------\r");
                Console.WriteLine("Выбор валюты для сравнения курса:\r");
                for (int i = 1; i < course.namesСurrency.Count; i++)
                {
                    Console.WriteLine(i +") " + course.namesСurrency[i]);
                }
                Console.WriteLine("Выбор валюты для сравнения курса:");
                var inputData = Console.ReadLine();
                int.TryParse(inputData, out int indexSelectedCourse);
                if ((indexSelectedCourse > (course.courses.Count-1)) ||  indexSelectedCourse < 0)
                {
                    Console.WriteLine("Неверный ввод.");                    
                }
                else
                {
                    Console.WriteLine("Ввод ожидаемого курса:");
                    inputData = Console.ReadLine();
                    double.TryParse(inputData, out double referenceValueCourse);
                    course.CompareCourse(indexSelectedCourse, referenceValueCourse);
                }         
                Console.WriteLine("End  Main Progrma");
            }
        }
    }
}
