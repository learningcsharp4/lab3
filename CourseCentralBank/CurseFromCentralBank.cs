﻿using System;
using System.Collections.Generic;
using HtmlAgilityPack;
using System.IO;

namespace TryCatchLesson
{
    /// <summary>
    /// Класс получения курса валют ЦБ.
    /// </summary>
    public class CurseFromCentralBank
    {
        #region Константы

        /// <summary>
        /// Адрес стартовой страници.
        /// </summary>
        private const string StartPageLink = @"https://www.cbr.ru/currency_base/daily/";

        #endregion

        #region Поля и свойства

        /// <summary>
        /// Делегат вывода данных.
        /// </summary>
        /// <param name="x">Аргумент делегата.</param>
        public delegate void PrintDel(List<string> x);

        /// <summary>
        /// Делегат вывода сообщений.
        /// </summary>
        /// <param name="message">Текст сообщения.</param>
        public delegate void MessegaeSender(string message);
        /// <summary>
        /// Событие выводящее в консоль сообщение.
        /// </summary>
        public event MessegaeSender Notify;

        /// <summary>
        /// Список курсов.
        /// </summary>
        public List<string> courses;

        /// <summary>
        /// Список названий ваолют.
        /// </summary>
        public List<string> namesСurrency;

        #endregion

        #region Методы

        /// <summary>
        /// Отправка сообщений в консоль.
        /// </summary>
        /// <param name="message">Текст сообщения.</param>
        public void Messege(string message) => Console.WriteLine(message);

        /// <summary>
        /// Получение курса валют ЦБ.
        /// </summary>
        /// <returns> Список валют.</returns>
        public List <string> GetExchangeRates()
        {
            var res = new List<string>();
            var htmlDoc = new HtmlWeb().Load(StartPageLink);
            var rows = htmlDoc.DocumentNode.SelectNodes("//table[@class='data']//tr");
            foreach (var cell in rows) { res.Add(cell.InnerText); }
            return res;
        }

        /// <summary>
        /// Вывод текущего курса в консоль.
        /// </summary>
        /// <param name="course">Список курсов.</param>
        public void PrintCourse(List<string> course)
        {
            foreach (var elements in course)
            {
                Console.WriteLine(elements);
            }
            Notify?.Invoke("Вывод курса завершен.");
        }

        /// <summary>
        /// Записать курс валют в файл.
        /// </summary>
        /// <param name="course">Список курса валют.</param>
        public void WriteCourseOnDevice(List<string> course)
        {
            try
            {
                DateTime thisDay = DateTime.Today;
                var nameWritingFile = "Course " + thisDay.ToString("d");
                string path = Directory.GetCurrentDirectory();
                var sw = new StreamWriter(path+"\\"+nameWritingFile + ".txt");
                foreach (var element in course)
                {
                    sw.WriteLine(element);
                }
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
        }

        /// <summary>
        /// Получение списка названий валют.
        /// </summary>
        public void FormatCourse()
        {
            foreach(var element in this.courses)
            {
                var courseData = GetStringInDataCourse(element,3);
                this.namesСurrency.Add(courseData);
            }
        }

        /// <summary>
        /// Извлечение подстроки с форматированием по заданному индексу.
        /// </summary>
        /// <param name="element">Массив значений 1 курса.</param>
        /// <param name="indexSubstring">Индекс(0 - Цифр. код, 1 - Букв.код, 2 - Единиц, 3 - Валюта, 4 - Курс).</param>
        /// <returns></returns>
        public string GetStringInDataCourse(string element, int indexSubstring)
        {
            var courseData = element;
            courseData = courseData.Trim();
            var courseDataArray = courseData.Split('\n');
            courseData = courseDataArray[indexSubstring];
            courseData = courseData.Trim();
            return courseData;
        }

        /// <summary>
        /// Сравнение курса с заданым значением.
        /// </summary>
        /// <param name="indexSelectedCourse">Индекс выбраного курса из списка.</param>
        /// <param name="referenceValueCourse">Заданное значение для сравнения.</param>
        public void CompareCourse(int indexSelectedCourse, double referenceValueCourse)
        {
            var actualCourse = GetStringInDataCourse(this.courses[indexSelectedCourse],4);
            double.TryParse(actualCourse, out double valueActualCourse);
            Console.WriteLine("Курс выбраной валюты " + valueActualCourse);
            var diffr = Math.Round((referenceValueCourse/valueActualCourse)*100-100);
            if (Math.Abs(diffr) >= 15) Notify("Курс отличается на 15%.");
        }

        #endregion

        /// <summary>
        /// Конструктор получения курса валют.
        /// </summary>
        #region Конструкторы
        public CurseFromCentralBank()
        {
            try
            {
                this.courses = GetExchangeRates();
                this.namesСurrency = new List<string>();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Исключение:");
                Console.WriteLine(ex.Message);
            }
        }
        #endregion
    }
}
